# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, fields
from trytond.pool import PoolMeta, Pool

__all__ = ['GroupGroup', 'Group',
           'ModelAccess', 'ModelFieldAccess']

__metaclass__ = PoolMeta


class GroupGroup(ModelSQL, ModelView):
    """Group - Group"""
    __name__ = "res.group-res.group"

    parent_group = fields.Many2One('res.group', 'Parent Group', ondelete='CASCADE',
                                   select=True, required=True)
    child_group = fields.Many2One('res.group', 'Group', ondelete='RESTRICT',
                                  select=True, required=True)

    @classmethod
    def __setup__(cls):
        super(GroupGroup, cls).__setup__()
        cls._sql_constraints += [
            ('group_uk1', 'UNIQUE(parent_group, child_group)',
                'You can not add a group twice!')
        ]

    @classmethod
    def create(cls, vlist):
        pool = Pool()
        _Group = pool.get('res.group')
        res = super(GroupGroup, cls).create(vlist)
        for item in res:
            if not any(m.group_origin and m.group_origin.id == item.child_group.id
                       for m in item.parent_group.model_access):
                _access = item.parent_group.explode_access()
                _Group.write([item.parent_group],
                             {'model_access': [('create', _access['model_access'])],
                              'field_access': [('create', _access['field_access'])]})
        return res

    @classmethod
    def delete(cls, groups):
        pool = Pool()
        _Group = pool.get('res.group')
        super(GroupGroup, cls).delete(groups)
        for group in groups:
            if any(m.group_origin and m.group_origin.id == group.child_group.id
                   for m in group.parent_group.model_access):
                _Group.write([group.parent_group],
                             {'model_access': [('remove',
                                                [m.id for m in group.parent_group.model_access if m.group_origin and
                                                 m.group_origin.id == group.child_group.id])],
                              'field_access': [('remove',
                                                [m.id for m in group.parent_group.field_access if m.group_origin and
                                                 m.group_origin.id == group.child_group.id])]
                             })


class Group:
    __name__ = 'res.group'

    groups = fields.Many2Many('res.group-res.group', 'parent_group', 'child_group',
                              'Groups')

    @fields.depends('groups', 'model_access', 'field_access')
    def on_change_groups(self):
        res = {'model_access': {'remove': [m.id for m in self.model_access if m.group_origin and
                                           m.group_origin.id not in [g.id for g in self.groups]],
                                'add': []},
               'field_access': {'remove': [m.id for m in self.field_access if m.group_origin and
                                           m.group_origin.id not in [g.id for g in self.groups]],
                                'add': []}
        }
        _access = self.explode_access()
        res['model_access']['add'].extend([(-1, item) for item in _access['model_access']])
        res['field_access']['add'].extend([(-1, item) for item in _access['field_access']])
        return res

    def explode_access(self):
        res = {'model_access': [],
               'field_access': []}
        for group in self.groups:
            if any(m.group_origin and m.group_origin.id == group.id for m in self.model_access):
                continue
            for m in group.model_access:
                values = self._explode_model_access(m)
                values['group_origin'] = m.group.id
                res['model_access'].append(values)
            for f in group.field_access:
                values = self._explode_model_access(f)
                values['group_origin'] = f.group.id
                res['field_access'].append(values)
        return res

    def _explode_model_access(self, access):
        values = {}
        for field_name, field in access._fields.iteritems():
            if field_name not in ('model', 'field', 'perm_read', 'perm_write',
                                  'perm_create', 'perm_delete', 'description'):
                continue
            try:
                value = getattr(access, field_name)
            except AttributeError:
                continue
            if value and field._type in ('many2one', 'one2one'):
                values[field_name] = value.id
            else:
                values[field_name] = value
        return values


class ModelAccess:
    __name__ = 'ir.model.access'

    group_origin = fields.Many2One('res.group', 'Group', ondelete='CASCADE')


class ModelFieldAccess:
    __name__ = 'ir.model.field.access'

    group_origin = fields.Many2One('res.group', 'Group', ondelete='CASCADE')